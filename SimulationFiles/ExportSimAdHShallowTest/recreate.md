ExportSimAdHShallow

To recreate test:
File-Open test2D.cmb from testing-data
File-Open AdHShallowWater.crf from testing-data
Switch to Attribute tab
Select "Functions" tab
Click New. Edit (double click) Row 3 to have x=3 f(x)=4
Select "Materials" tab
Click New. Click below scroll bar to associations
Associate Face1 then Face3
Select "Boundary Conditions" tab
Select "Unit Flow BC" from dropdown, click New
Associated Edge 5, then Edge 2
Click the down arrow on the side tabs
Select "Extrusions" tab
Click New, associate Face 3
File-Export Simulation File
Select "CFD Flow"
Change Level to Advanced
Select output.2dm from testing-data for Analysis Mesh File
Select output.hot from testing-data for Hot Start File
Save to test.bc in the testing-data folder.
 * Note: Manually edit the script afterward to point to testing/Temporary
Click the down button in the scrollbar
Select "AdHShallowWater.py" from testing-data as "Python Script"
Export and Stop Recording
