ExportSimHydra

To recreate test:
File-Open simplebox.cmb from testing-data
File-Open Hydra_Template.sbt from testing-data

Select "Attribute" tab
Click New. Click below scrollbar once
Associate Piece 0 with the material
Select "Source Terms" tab
Select "Heat Source" attribute type
Click New. Associate "Piece 0" with the attribute.
Set Scale to 1
Select "Execution" tab
Set "Frequency" in "Field Output" to 25
Select "Problem Definition" tab
Check "Use exact termination time"
Select "Field Output"
Select "Element Time History Output"
Click New. Set id=0 and Variable Name="density"
Click the down arrow below the tabs 3 times
Select "Boundary Conditions" tab
Change Attribute type to "Temperature"
Click New.
Set scale=1 and associate Face2
Select "Functions" tab
Click New.
For row 5, set x=4 f(x)=3

File-Export Simulation File
Check "Incompressible Navier-Stokes Analysis"
Select "HydraExporter.py" from testing-data as "Python Script"
Save to test.bc in the testing-data folder.
 * Note: Manually edit the script afterward to point to testing/Temporary
Export and Stop Recording
