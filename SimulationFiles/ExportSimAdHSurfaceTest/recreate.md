ExportSimAdHSurface

To recreate test:
File-Open test2D.cmb from testing-data
File-Open AdHSurfaceWater.crf from testing-data
Switch to Attribute tab
Select "Functions" tab
Click New. Edit (double click) Row 3 to have x=3 f(x)=4
Select "Materials" tab
Click New. Click below scroll bar to associations
Associate Face1 then Face3
Select "Boundary Conditions" tab
Select "Total Discharge BC" from dropdown, click New
Associate Edge 5, then Edge 2
Select "Floating Stationary BC" from dropdown, click New
Associate Edge1, then Edge 8
File-Export Simulation File
Select "Constituent Transport"
Change Level to Advanced
Save to test.bc in the testing-data folder.
 * Note: Manually edit the script afterward to point to testing/Temporary
Click the down button in the scrollbar
Select "AdHSurfaceWater.py" from testing-data as "Python Script"
Export and Stop Recording
