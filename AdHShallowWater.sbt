<SimBuilder Version="2.1">

  <AnalysisGroups>
    <Instance TagName="General" UIName="General"/>
    <Instance TagName="Constituent Transport" UIName="Constituent Transport"/>
    <Instance TagName="Sediment Transport" UIName="Sediment Transport"/>
    <Instance TagName="Shallow Water" UIName="Shallow Water Flow"/>
  </AnalysisGroups>

  <Maps>
    <Instance TagName="ConstituentMap" UIName="Constituent"/>
  </Maps>

  <TopLevelGroup>
    <TopLevel Section="Functions" UIName="Functions" Icon="Function.png">
      <Templates>
        <Instance TagName="Function1DLinear"/>
      </Templates>
    </TopLevel>

    <TopLevel Section="Constituents" UIName="Constituents" Icon="Constituent.png">
      <Templates Map="ConstituentMap">
        <Instance TagName="GeneralConstituent" UIName="General constituent">
          <InformationValue TagName="GenConstituentParams" UIName="General constituent transport parameters:">
            <Group Name="Constituent Transport"/>
            <ValueType Name="double"/>
            <DefaultValue Value="1"/>
          </InformationValue>
        </Instance>
        <Instance TagName="VortConstituent" UIName="Vorticity constituent">
          <InformationValue TagName="VortConstituentParams" UIName="Vorticity transport parameters:">
            <Group Name="Constituent Transport"/>
            <MultiValue>
              <InputValue Index="1">
                <ValueType Name="double"/>
                <DefaultValue Value="1"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="2">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              <InputValue Index="3">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
            </MultiValue>
          </InformationValue>
        </Instance>
        <Instance TagName="SalinityConstituent" UIName="Salinity constituent">
          <InformationValue TagName="SalConstituentParams" UIName="Salinity transport parameters:">
            <Group Name="Constituent Transport"/>
            <ValueType Name="double"/>
            <DefaultValue Value="1"/>
          </InformationValue>
        </Instance>
        <Instance TagName="TemperatureConstituent" UIName="Temperature constituent">
          <InformationValue TagName="TempConstituentParams" UIName="Temperature transport parameters:">
            <Group Name="Constituent Transport"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="0"/>
          </InformationValue>
        </Instance>
      </Templates>
    </TopLevel>

    <TopLevel Section="Materials" UIName="Materials">
      <Templates>
        <Instance TagName="SolidMaterial" UIName="Material Template">
          <InformationValue TagName="TurbulentDiffusionRate" UIName="Turbulent diffusion rate:" Map="ConstituentMap">
            <Group Name="Constituent Transport"/>
            <ValueType Name="double"/>
            <DefaultValue Value="1.0E-06"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InformationValue>
          <InformationValue TagName="KinematicEddyViscosity" UIName="Kinematic eddy viscosity:">
            <Group Name="General"/>
            <MultiValue>
              <InputValue Index="1">
                <ValueType Name="double"/>
                <DefaultValue Value=".5"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="2">
                <ValueType Name="double"/>
                <DefaultValue Value=".5"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="3">
                <ValueType Name="double"/>
                <DefaultValue Value=".5"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
            </MultiValue>
          </InformationValue>
          <InformationValue TagName="CoriolisLatitude" UIName="Coriolis Latitude:">
            <Group Name="General"/>
            <ValueType Name="double"/>
            <DefaultValue Value="0"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="-90"/>
            <Constraint Type="Maximum" Inclusive="Yes" Value="90"/>
          </InformationValue>
	  <InformationValue TagName="MaxRefineLevels" UIName="Maximum Levels of Refinement:">
            <Group Name="General"/>
            <Advanced/>
            <ValueType Name="int"/>
            <DefaultValue Value="0"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
	  </InformationValue>
	  <InformationValue TagName="HydroRefineTol" UIName="Hydro refinement tolerance:">
            <Group Name="Shallow Water"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="1"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
	  </InformationValue>
	  <InformationValue TagName="TransportRefineTol" UIName="Transport refinement tolerance:">
            <Group Name="Constituent Transport"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="1"/>
            <Option Default="OFF"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
	  </InformationValue>
        </Instance>
      </Templates>
    </TopLevel>

    <TopLevel Section="Domain" UIName="Domain">
      <InformationValue TagName="DomainDimension" UIName="Domain Dimension">
        <ValueType Name="int"/>
        <DefaultValue Value="2"/>
        <Constraint Type="Minimum" Inclusive="Yes" Value="2"/>
        <Constraint Type="Maximum" Inclusive="Yes" Value="3"/>
      </InformationValue>
      <Templates>
        <Instance TagName="DomainMaterialAssignment"/>
      </Templates>
    </TopLevel>

    <TopLevel Section="Analysis" UIName="Analysis">
      <Group Name="Constituent Transport" Active="1"/>
      <Group Name="Sediment Transport" Active="1"/>
      <Group Name="Shallow Water" Active="1"/>
      <Group Name="General" Active="1"/>
      <Templates>
        <!-- shallow water flow bcs -->
        <Instance Name="Dirichlet velocity">
          <InformationValue TagName="VelocityBound" UIName="Dirichlet velocity:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Nodal"/>
            <MultiValue>
              <InputValue Index="1">
                <ValueType Name="FunctionId"/>
              </InputValue>
              <InputValue Index="2">
                <ValueType Name="FunctionId"/>
              </InputValue>
            </MultiValue>
          </InformationValue>
        </Instance>
        <Instance Name="Dirichlet velocity and depth">
          <InformationValue TagName="VelocityAndDepthBound" UIName="Dirichlet velocity and depth:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Nodal"/>
            <MultiValue>
              <InputValue Index="1">
                <ValueType Name="FunctionId"/>
              </InputValue>
              <InputValue Index="2">
                <ValueType Name="FunctionId"/>
              </InputValue>
              <InputValue Index="3">
                <ValueType Name="FunctionId"/>
              </InputValue>
            </MultiValue>
          </InformationValue>
        </Instance>
        <Instance Name="Stationary lid elevation">
          <InformationValue TagName="LidElevation" UIName="Stationary lid elevation:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Nodal"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
        <Instance Name="Water depth under stationary lid">
          <InformationValue TagName="WaterDepthLid" UIName="Water depth under stationary lid:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Nodal"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
        <Instance Name="Floating stationary object">
          <InformationValue TagName="FloatingStationary" UIName="Floating stationary object:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Nodal"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
        <Instance Name="Total discharge">
          <InformationValue TagName="TotalDischarge" UIName="Total discharge:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Nodal"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
        <Instance Name="Unit flow">
          <InformationValue TagName="UnitFlow" UIName="Unit flow:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Boundary"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
        <Instance Name="Water surface elevation">
          <InformationValue TagName="WaterSurfElev" UIName="Water surface elevation:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Boundary"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
        <Instance Name="Stage discharge boundary">
          <InformationValue TagName="StageDischargeBound" UIName="Stage discharge boundary:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Boundary"/>
            <MultiValue>
              <InputValue Index="1">
                <ValueType Name="double"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="2">
                <ValueType Name="double"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="3">
                <ValueType Name="double"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="4">
                <ValueType Name="double"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="5">
                <ValueType Name="double"/>
                <Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
            </MultiValue>
          </InformationValue>
        </Instance>
        <Instance Name="Spillway boundary">
          <InformationValue TagName="SpillwayBound" UIName="Spillway boundary:">
            <Group Name="Shallow Water"/>
            <BoundaryConditionGroup Name="Boundary"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
        <Instance Name="Outflow boundary">
          <InformationValue TagName="OutflowBound" UIName="Outflow boundary:">
            <BoundaryConditionGroup Name="Boundary"/>
            <Group Name="Shallow Water"/>
            <ValueType Name="void"/>
          </InformationValue>
        </Instance>
        <!-- constituent transport bcs -->
        <Instance Name="Dirichlet transport">
          <InformationValue TagName="Dirichlet transport" UIName="Dirichlet transport:" Map="ConstituentMap">
            <BoundaryConditionGroup Name="Nodal"/>
            <Group Name="Constituent Transport"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
        <Instance Name="Natural Transport">
          <InformationValue TagName="Natural transport" UIName="Natural transport:" Map="ConstituentMap">
            <BoundaryConditionGroup Name="Boundary"/>
            <Group Name="Constituent Transport"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
        </Instance>
      </Templates>
    </TopLevel>
    <TopLevel Section="Time" UIName="Time">
      <InformationValue TagName="TimestepSize" UIName="Timestep size:">
        <Description Value="Index to the desired time step size series"/>
        <Group Name="General"/>
        <ValueType Name="FunctionId"/>
      </InformationValue>
      <InformationValue TagName="StartTime" UIName="Start Time:">
        <Group Name="General"/>
        <MultiValue>
          <InputValue Index="1">
            <ValueType Name="double"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
          </InputValue>
          <InputValue Index="2">
            <ValueType Name="int"/>
            <Constraint Type="Discrete" DefaultIndex="0">
              <Enum Value="0" Name="Seconds"/>
              <Enum Value="1" Name="Minutes"/>
              <Enum Value="2" Name="Hours"/>
              <Enum Value="3" Name="Days"/>
              <Enum Value="4" Name="Weeks"/>
            </Constraint>
          </InputValue>
        </MultiValue>
      </InformationValue>
      <InformationValue TagName="EndTime" UIName="End Time:">
        <Group Name="General"/>
        <MultiValue>
          <InputValue Index="1">
            <ValueType Name="double"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
          <InputValue Index="2">
            <ValueType Name="int"/>
            <Constraint Type="Discrete" DefaultIndex="0">
              <Enum Value="0" Name="Seconds"/>
              <Enum Value="1" Name="Minutes"/>
              <Enum Value="2" Name="Hours"/>
              <Enum Value="3" Name="Days"/>
              <Enum Value="4" Name="Weeks"/>
            </Constraint>
          </InputValue>
        </MultiValue>
      </InformationValue>
      <InformationValue TagName="SteadyStateSolveParams" UIName="Steady state solve parameters:">
        <Group Name="General"/>
        <ValueType Name="double"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="QuasiUnsteadyParams" UIName="Parameters defining quasi-unsteady simulations:">
        <Option Default="OFF"/>
        <Group Name="General"/>
        <Advanced/>
        <MultiValue>
          <InputValue Index="1">
            <ValueType Name="int"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
          <InputValue Index="2">
            <ValueType Name="int"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
          <InputValue Index="3">
            <ValueType Name="double"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
        </MultiValue>
      </InformationValue>
      <InformationValue TagName="AutoTimeStepFind" UIName="Automatic timestep find:">
        <Option Default="OFF"/>
        <Group Name="General"/>
        <MultiValue>
          <InputValue Index="1">
            <ValueType Name="double"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
          <InputValue Index="2">
            <ValueType Name="int"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
        </MultiValue>
      </InformationValue>
      <InformationValue TagName="PrintAdaptedMeshes" UIName="Print adapted meshes:">
        <Group Name="General"/>
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="HotStartFile" UIName="Hotstart file print control:">
        <Group Name="General"/>
        <ValueType Name="int"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="PrintStandardOutput" UIName="Print standard output using the previous format:">
        <Group Name="General"/>
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      <!--
          <InformationValue TagName="OutputSeriesSpec" UIName="Output series specification:">
            <Group Name="General"/>
            <ValueType Name="FunctionId"/>
          </InformationValue>
          -->
      <InformationValue TagName="AutoBuildSeries" UIName="Autobuild output series:">
        <Group Name="General"/>
        <MultiValue>
          <InputValue Index="1">
            <ValueType Name="FunctionId"/>
          </InputValue>
          <InputValue Index="2">
            <ValueType Name="int" MultiTag="NumOutputSeries"/>
            <DefaultValue Value="1"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
          <InputValue Index="3">
            <ValueType Name="int"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
            <Constraint Type="Maximum" Inclusive="No" Value="5"/>
          </InputValue>
        </MultiValue>
      </InformationValue>
      <InformationValue TagName="AutoBuildFuncDef" UIName="Autobuild output series function definition:" MultiRef="NumOutputSeries">
        <Group Name="General"/>
        <MultiValue>
          <InputValue Index="1">
            <ValueType Name="double"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
          <InputValue Index="2">
            <ValueType Name="double"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
          <InputValue Index="3">
            <ValueType Name="double"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
          </InputValue>
          <InputValue Index="4">
            <ValueType Name="int"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
            <Constraint Type="Maximum" Inclusive="No" Value="5"/>
          </InputValue>
        </MultiValue>
      </InformationValue>
      <!--
          <InformationValue TagName="StringFluxComp" UIName="Flux computation across the specified string:">
            <Group Name="General"/>
            <ValueType Name="int"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
            <Option Default="OFF"/>
          </InformationValue>
          <InformationValue TagName="RemoveSpecifiedString" UIName="Remove specified string from the computations:">
            <Group Name="General"/>
            <ValueType Name="int"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
            <Option Default="OFF"/>
          </InformationValue>
          -->
    </TopLevel>
    <TopLevel Section="Globals" UIName="Globals">
      <InformationValue TagName="Gravity" UIName="Gravity:" Units="m/s^2">
        <Group Name="General"/>
        <ValueType Name="double"/>
        <DefaultValue Value="9.80"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      <InformationValue TagName="KinMolViscosity" UIName="Kinematic molecular viscosity:">
        <Group Name="General"/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.0E-06"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      <InformationValue TagName="ReferenceDensity" UIName="Fluid density:">
        <Group Name="General"/>
        <ValueType Name="double"/>
        <DefaultValue Value="1000"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      <InformationValue TagName="ManningsUnitConstant" UIName="Manning's unit constant (1.0 for SI, 1.486 for English):">
        <Group Name="General"/>
        <ValueType Name="double"/>
        <DefaultValue Value="1"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      <InformationValue TagName="WetDryLimits" UIName="Wetting/drying limits:">
        <Group Name="General"/>
        <ValueType Name="double"/>
        <DefaultValue Value="0"/>
        <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
      </InformationValue>

    </TopLevel>
    <TopLevel Section="Solvers" UIName="Solvers">
      <InformationValue TagName="MemoryIncrementBlockSize" UIName="Block size for memory increment:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="int"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
        <DefaultValue Value="40"/>
      </InformationValue>
      <InformationValue TagName="PreconditioningBlocks" UIName="Subdomain blocks per processor for preconditioning:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="int"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
        <DefaultValue Value="1"/>
      </InformationValue>
      <InformationValue TagName="PreconditionerType" UIName="Preconditioner Type:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="int"/>
        <Constraint Type="Discrete" DefaultIndex="0">
          <Enum Value="0" Name="None"/>
          <Enum Value="1" Name="1 Level Additive Schwartz"/>
          <Enum Value="2" Name="2 Level Additive Schwartz"/>
          <Enum Value="3" Name="2 Level Hybrid"/>
        </Constraint>
      </InformationValue>
      <InformationValue TagName="TemporalSchemeCoefficient" UIName="Coefficient for the second order temporal scheme:">
        <Group Name="General"/>
        <Advanced/>
        <Option Default="OFF"/>
        <ValueType Name="double"/>
        <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
        <Constraint Type="Maximum" Inclusive="Yes" Value="1"/>
        <DefaultValue Value="0"/>
      </InformationValue>
      <InformationValue TagName="PetrovGalerkinCoefficient" UIName="Coefficient for the Petrov-Galerkin equation:">
        <Group Name="General"/>
        <Advanced/>
        <Option Default="OFF"/>
        <ValueType Name="double"/>
        <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
        <Constraint Type="Maximum" Inclusive="Yes" Value=".5"/>
        <DefaultValue Value="0"/>
      </InformationValue>
      <InformationValue TagName="VesselMovement" UIName="Enable vessel movement:">
        <Group Name="General"/>
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="VesselEntrainment" UIName="Enable vessel entrainment:">
        <Group Name="General"/>
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="SW2Gradients" UIName="Compute SW2 gradients:">
        <Advanced/>
        <Group Name="General"/>
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="NonLinearTolMaxNorm" UIName="Non-Linear Tolerance (Max Norm):">
        <Group Name="General"/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.0E-04"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      <InformationValue TagName="NonLinearTolMaxChange" UIName="Non-Linear Tolerance (Maximum Change in Iteration):">
        <Option Default="OFF"/>
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.0E-02"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      <InformationValue TagName="MaxNonLinearIters" UIName="Maximum nonlinear iterations per time step:">
        <Group Name="General"/>
        <ValueType Name="int"/>
        <DefaultValue Value="6"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      <InformationValue TagName="MaxLinearIters" UIName="Maximum linear iterations per nonlinear iteration:">
        <Group Name="General"/>
        <ValueType Name="int"/>
        <DefaultValue Value="200"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      <InformationValue TagName="RungeKuttaTol" UIName="Runge-Kutta tolerance for reactive constituents:">
        <Advanced/>
        <Group Name="General"/>
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="HydrodynamicsTol" UIName="Quasi-unsteady tolerance for hydrodynamics:">
        <Group Name="General"/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.0E-04"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
    </TopLevel>
  </TopLevelGroup>

  <Libraries>
  </Libraries>


</SimBuilder>

